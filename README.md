**Adlab // McCann**

Fully responsive remake of the Adlab//McCann website (2018).

*Created as a proposal to replace the [current Adlab website](http://adlabmccann.com)*

---

## Comments

Feel free to share your opinions/suggestions with the Developer by commenting on this repository here on BitBucket.


## Credit

Re-designed and developed from the ground up by [Frank Simmons](https://www.linkedin.com/in/frank-simmons-51459b26/), using:

1. Code IDE **Microsoft Visual Studio Code**.
2. Framework **UIkit**..
3. HTML5 template **HTML5 boilerplate**.